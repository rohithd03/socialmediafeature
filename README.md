# Report feature in Social Media

***

## Description

* The Only user logged in is Alice.
* She can like or dislike the default posts by dummy users.
* She can report the post by submitting a form.

***

## Installation

Clone the project or download the project.
```
npm i
```
Install all the dependencies.

***

## env file
Add an .env file with the details
```
DB_URL = "MongoDB url"
PORT = "Port number"
```
If not mentioned these are set to default values.

***

## Backend

```
npm start
```
Starts the application at http://localhost:5000/

***

## Frontend

```
cd client
npm i
npm start
```
Starts the application at http://localhost:3000/

***

## Backend Route

### Post /post/:pid/report
```
{
    "reason": "Reason specified by user",
    "pid": "Post id",       // pid here
    "caption": "Post caption",
    "author": "Post author name",
    "image": "Post image url",
    "reportedby": "Username who reported"
}
```

***

## Gallery
![Screenshot1](images/ss1.png)