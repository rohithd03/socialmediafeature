import './App.css';

import * as React from 'react';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardMedia from '@mui/material/CardMedia';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions';
import ChatBubbleRoundedIcon from '@mui/icons-material/ChatBubbleRounded';
import BookIcon from '@mui/icons-material/Book';
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import { red } from '@mui/material/colors';
import FavoriteIcon from '@mui/icons-material/Favorite';
import ShareIcon from '@mui/icons-material/Share';
import Tooltip from '@mui/material/Tooltip';
import MoreHorizIcon from '@mui/icons-material/MoreHoriz';
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import { makeStyles } from "@material-ui/core/styles";
import Box from "@material-ui/core/Box";
import { styled, alpha } from '@mui/material/styles';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import axios from 'axios';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

const useStyles = makeStyles((theme) => ({
    paper: {
      padding: "5rem"
    }
  }));

  const StyledMenu = styled((props) => (
    <Menu
      elevation={0}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'right',
      }}
      {...props}
    />
  ))(({ theme }) => ({
    '& .MuiPaper-root': {
      borderRadius: 6,
      marginTop: theme.spacing(1),
      minWidth: 180,
      color:
        theme.palette.mode === 'light' ? 'rgb(55, 65, 81)' : theme.palette.grey[300],
      boxShadow:
        'rgb(255, 255, 255) 0px 0px 0px 0px, rgba(0, 0, 0, 0.05) 0px 0px 0px 1px, rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px',
      '& .MuiMenu-list': {
        padding: '4px 0',
      },
      '& .MuiMenuItem-root': {
        '& .MuiSvgIcon-root': {
          fontSize: 18,
          color: theme.palette.text.secondary,
          marginRight: theme.spacing(1.5),
        },
        '&:active': {
          backgroundColor: alpha(
            theme.palette.primary.main,
            theme.palette.action.selectedOpacity,
          ),
        },
      },
    },
  }));

export default function Post(props) {

    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const [open, setOpen] = React.useState(false);
    const moreOpen = Boolean(anchorEl);
    const [message, setMessage] = React.useState("");
    const [status, Setstatus] = React.useState(false);
    const [like, setLike] = React.useState(false);

    const handleOpen = () => {
        setOpen(true);
        handleMoreClose()
    };

    const handleClose = () => {
        setOpen(false);
        setMessage("");
        Setstatus(false);
    };

    const handlelike = () => {
        setLike(!like);
    }

    const handleMoreOpen = (e) => {
        setAnchorEl(e.currentTarget);
    };

    const handleMoreClose = () => {
        setAnchorEl(null);
    };

    const handleSubmit = async (e) => {
        e.preventDefault()
        const report = {
            reason: e.target.reason.value,
            pid: props.id,
            caption: props.caption,
            author: props.name,
            image: props.image,
            reportedby: "Alice"
        }
        let url = 'http://localhost:5000/post/' + report.pid + "/report"
        await axios.post(url, report)
        .then(res => {
            console.log(res);
            console.log(res.data);
            setMessage(res.data.message)
            Setstatus(true)
        })
        e.target.reason.value = ""
    }

  return (
    <div className="Card">
        <Card sx={{ maxWidth: 345 }}>
        <CardHeader
            avatar={
            <Avatar sx={{ bgcolor: red[500] }} aria-label="recipe" src={props.dp}>
                {props.dp}
            </Avatar>
            }
            action={
            <IconButton
                id="more-button"
                aria-label="settings"
                aria-controls={moreOpen ? 'more-menu' : undefined}
                aria-haspopup="true"
                aria-expanded={moreOpen ? 'true' : undefined}
                onClick={handleMoreOpen}
            >
                <MoreHorizIcon />
            </IconButton>
            }
            title={props.name}
        />
        <CardMedia
            component="img"
            width="160"
            image={props.image}
            alt="Paella dish"
        />
        <CardActions disableSpacing>
            <Tooltip title={like ? "Unlike" : "Like"}>
                <IconButton aria-label="add to favorites" onClick={handlelike}>
                    <FavoriteIcon sx={{color:(like ? "#F47C7C" : "")}} />
                </IconButton>
            </Tooltip>
            <Tooltip title="Comment">
                <IconButton aria-label="comment">
                    <ChatBubbleRoundedIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Share">
                <IconButton aria-label="share">
                    <ShareIcon />
                </IconButton>
            </Tooltip>
            <Tooltip title="Save">
                <IconButton aria-label="save" sx={{marginLeft: "auto"}}>
                    <BookIcon />
                </IconButton>
            </Tooltip>
        </CardActions>
        <CardContent sx={{padding: "0rem 1rem"}}>
            <Typography variant="body1" color="text.primary">
                <strong> {like ? parseInt(props.likes, 10)+1 : props.likes} likes </strong>
            </Typography>
            <Typography variant="body2" color="text.primary">
                <strong>{props.name}</strong> {" "} {props.caption}
            </Typography>
        </CardContent>
        
        </Card>
        <Dialog open={open}>
            <Box className={classes.paper}>
                <div style={{textAlign:"center"}}>
                    {status && <CheckCircleIcon fontSize="large" sx={{color:"green"}} />}
                </div>
                {message}
            <form onSubmit={handleSubmit}>
                <div>
                    Why do you want to report?
                </div>
                <div>
                    <TextField variant="standard" name="reason" required={true} helperText="Enter a reason..." />
                </div>
                <div>
                    {status ? <Button type="submit" disabled>Submit</Button> : <Button type="submit">Submit</Button>}
                    <Button onClick={handleClose}>Cancel</Button>
                </div>
            </form>
            </Box>
        </Dialog>
        <StyledMenu
                id="more-menu"
                MenuListProps={{
                    'aria-labelledby': 'more-button',
                }}
                anchorEl={anchorEl}
                open={moreOpen}
                onClose={handleMoreClose}
            >
                <MenuItem onClick={handleOpen} disableRipple>
                    Report
                </MenuItem>
        </StyledMenu>
    </div>
  );
}
