const data = [
    {
        id: 1,
        dp: 'B',
        name: 'Bob',
        likes: '10',
        caption: 'My new Car!!',
        image: 'images/car1.jpg'
    },
    {
        id: 2,
        dp: 'B',
        name: 'Bob',
        likes: '100',
        caption: 'Hottest Car!!',
        image: 'images/hotcar.jpg'
    },
    {
        id: 3,
        dp: 'images/rohith.jpg',
        name: 'Rohith',
        likes: '43',
        caption: 'My fav dish',
        image: 'images/dish1.jpg'
    },
    {
        id: 4,
        dp: 'B',
        name: 'Bob',
        likes: '157',
        caption: 'Love Jaguar',
        image: 'images/jaguar.jpg'
    },
    {
        id: 5,
        dp: 'C',
        name: 'Charlie',
        likes: '248',
        caption: 'My sports new Car!!',
        image: 'images/sportscar.jpg'
    }
]


export default data;