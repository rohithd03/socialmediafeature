// import logo from './logo.svg';
import './App.css';
import Navbar from "./Navbar.js";
import Post from "./Post.js";
import data from "./data.js"

function App() {
  return (
    <div>
      <div className="App">
        <Navbar />
      </div>
      <div className="Posts">
        {data.map((d, i) => {
          return <Post key={i} id={d.id} dp={d.dp} name={d.name} likes={d.likes} caption={d.caption} image={d.image} />
        })}
      </div>
    </div>
  );
}

export default App;
