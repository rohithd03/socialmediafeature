const mongoose = require("mongoose");

const reportSchema = new mongoose.Schema({
    reason: {
        type: String,
        required: true
    },
    postID: {
        type: String,
        required: true,
        unique: true
    },
    postCaption: {
        type: String,
        required: true
    },
    postAuthor: {
        type: String,
        required: true
    },
    postImage: {
        type: String,
        required: true
    },
    reportedBy: {
        type: String,
        required: true
    }
}, {timestamps: {createdAt: "created_at", updatedAt: "updated_at"}})

module.exports = mongoose.model("Report", reportSchema);