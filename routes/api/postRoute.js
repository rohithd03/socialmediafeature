const express = require("express");
const app = express.Router();
const Report = require("../../models/report");

app.get("/ping", (req, res) => {
    res.send("Hello world from posts!!");
})

app.post("/:pid/report", async (req, res, next) => {
    try {
        let result = await Report.findOne({postID: req.params.pid, reportedBy: req.body.reportedby})
        if(result){
            return res.status(200).send({"Success": "true", "message": "You've alredy reported on this post!"})
        }
        let report = new Report({
            reason: req.body.reason,
            postID: req.params.pid,
            postCaption: req.body.caption,
            postAuthor: req.body.author,
            postImage: req.body.image,
            reportedBy: req.body.reportedby
        })

        await report.save()

        return res.status(200).send({"Success": "true", "message": "Your report has been saved!!", "reportData": report});

    } catch (error) {
        next(error)
    }
})

module.exports = app;