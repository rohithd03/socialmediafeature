require('dotenv').config();
const express = require("express");
const mongoose = require("mongoose");
const cors = require('cors');

const app = express();
const postRouter = require("./routes/api/postRoute.js");

app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(cors());

app.use("/post", postRouter)

app.get("/ping", (req, res) => {
    res.send("Hello world!");
})

const PORT = process.env.PORT || 5000;

const uri = process.env.DB_URL || "mongodb://localhost:27017/mds";

async function connectDB(){
   mongoose.connect(uri, {useNewUrlParser: true});
}

app.listen(process.env.PORT || PORT, async function(){
  await connectDB();
  console.log(`Server started at port ${PORT}`);
});

process.on('unhandledRejection', error => {
  console.log('unhandledRejection', error);
});

process.on('uncaughtException', function (err) {
  console.error(err);
});
